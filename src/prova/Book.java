package prova;

public class Book extends Item{
	

	private String autor;
	private String edicao;
	private String volume;
	/**
	 * @return the autor
	 * 
	 */
	public Book(String title, String editora, int ano, String isbn, double preco, String autor, String edicao, String volume) {
		super(title, editora, ano, isbn, preco);
		
		this.autor = autor;
		this.edicao = edicao;
		this.volume = volume;
		// TODO Auto-generated constructor stub
	}
	public String getAutor() {
		return autor;
	}
	/**
	 * @param autor the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}
	/**
	 * @return the edicao
	 */
	public String getEdicao() {
		return edicao;
	}
	/**
	 * @param edicao the edicao to set
	 */
	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}
	/**
	 * @return the volume
	 */
	public String getVolume() {
		return volume;
	}
	/**
	 * @param volume the volume to set
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	public void Display() {
		
		System.out.println("title: " + getTitle() + ", Editora: " + getEditora() + ", Ano de lan�amento: "+ getAno() + 
				", codigo do livro: "+ getIsbn() + ", Pre�o: " + getPreco() + ", Autor: " + this.autor + ", Edi��o: " + this.edicao + ", Volume: "+ this.volume);
	}
}
