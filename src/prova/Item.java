package prova;


public class Item {
	private String title;
	private String editora;
	private int ano;
	private String isbn;
	private double preco;
	/**
	 * @return the title
	 */
	
	public Item(String title, String editora, int ano, String isbn, double preco ) {
		this.title = title;
		this.editora = editora;
		this.ano = ano;
		this.isbn = isbn;
		this.preco = preco;
	}
	
	
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the editora
	 */
	public String getEditora() {
		return editora;
	}
	/**
	 * @param editora the editora to set
	 */
	public void setEditora(String editora) {
		this.editora = editora;
	}
	/**
	 * @return the ano
	 */
	public int getAno() {
		return ano;
	}
	/**
	 * @param ano the ano to set
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}
	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	/**
	 * @return the preco
	 */
	public double getPreco() {
		return preco;
	}
	/**
	 * @param preco the preco to set
	 */
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public void Display() {
		System.out.println("title = " + getTitle() + ", Editora =  " + getEditora() + ", Ano de lan�amento = "+ getAno() + 
				", codigo do livro =  "+ getIsbn() + ", Pre�o = " );
	}
	
}
